#!/bin/bash
if [ -z "$SBT_VERSION" ]
then
  SBT_VERSION=0.13.0
fi

SBT_LAUNCH_JAR=`dirname $0`/sbt-launch-$SBT_VERSION.jar
if [ ! -r $SBT_LAUNCH_JAR ]
then
  TYPESAFE_URL="http://repo.typesafe.com/typesafe/ivy-releases/org.scala-sbt/sbt-launch/$SBT_VERSION/sbt-launch.jar"
  echo "Downloading sbt $SBT_VERSION from $TYPESAFE_URL"
  wget -q -O "$SBT_LAUNCH_JAR" $TYPESAFE_URL

  if [ ! -r $SBT_LAUNCH_JAR ]
  then
    echo "Unable to download file."
  fi
fi

SBT_OPTS="-Xms512M -Xmx1024M -XX:MaxPermSize=256M"
java $SBT_OPTS -jar $SBT_LAUNCH_JAR "$@"
