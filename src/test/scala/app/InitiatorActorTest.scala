package app

import java.net.InetSocketAddress

import akka.actor.{Props, ActorSystem}
import akka.testkit.{TestKitBase, TestProbe, TestKit}
import io.ClientActor
import org.scalatest.FreeSpec
import utils.Utils._

class InitiatorActorTest extends FreeSpec with TestKitBase {

  implicit lazy val system = ActorSystem("Test")

  trait Fixture {
    val clientActor = TestProbe()
    val sendLogActor = TestProbe()
    val receiveLogActor = TestProbe()
    val generator = new IntsGenerator(null) {
      override def run(): Unit = {}
    }

    val initiator = system.actorOf(Props(new InitiatorActor(new InetSocketAddress("test", 99)) {
      override val client = clientActor.ref
      override val sendLog = sendLogActor.ref
      override val receiveLog = receiveLogActor.ref
      override val generator = Fixture.this.generator
    }))

    val sequenceBuffer = system.actorOf(Props(new SequenceBufferActor("0", initiator)))
  }

  "should emit ints after start command" in new Fixture {
    initiator ! 0L
    clientActor.expectNoMsg()
    sendLogActor.expectNoMsg()

    initiator ! Start

    initiator ! 0L

    clientActor.expectMsg(ClientActor.Send("0"))
    sendLogActor.expectMsg("0")
  }

  "should stop ints emitting after stop command" in new Fixture {
    initiator ! Start

    initiator ! 0L

    initiator ! Stop

    initiator ! 1L

    clientActor.expectMsg(ClientActor.Send("0"))
    clientActor.expectMsg(ClientActor.Send(STOP_MSG))
    sendLogActor.expectMsg("0")

    clientActor.expectNoMsg()
    sendLogActor.expectNoMsg()
  }

  "should wait last int after exit" in new Fixture {
    val watcher = TestProbe()
    watcher watch initiator

    initiator ! Start

    initiator ! 0L
    initiator ! 1L
    initiator ! 2L
    initiator ! ClientActor.Received("0")

    initiator ! Exit

    receiveLogActor.expectMsg("0")
    receiveLogActor.expectNoMsg()

    initiator ! ClientActor.Received("1")
    initiator ! ClientActor.Received("2")

    receiveLogActor.expectMsg("1")
    receiveLogActor.expectMsg("2")

    watcher.expectTerminated(initiator)
  }

  "should wait all ints sequense before stop" in new Fixture {
    val watcher = TestProbe()
    watcher watch initiator

    initiator ! Start

    initiator ! 0L
    initiator ! 1L
    initiator ! 2L

    initiator ! Exit

    sequenceBuffer ! ClientActor.Received("2")
    sequenceBuffer ! ClientActor.Received("0")

    watcher.expectNoMsg()

    sequenceBuffer ! ClientActor.Received("1")
    watcher.expectTerminated(initiator)
  }

  "should write ints in descent order" in new Fixture {
    initiator ! Start

    initiator ! 0L
    initiator ! 1L
    initiator ! 2L

    sendLogActor.expectMsg("0")
    sendLogActor.expectMsg("1")
    sendLogActor.expectMsg("2")

    sequenceBuffer ! ClientActor.Received("2")
    receiveLogActor.expectNoMsg()

    sequenceBuffer ! ClientActor.Received("0")
    receiveLogActor.expectMsg("0")

    sequenceBuffer ! ClientActor.Received("1")
    receiveLogActor.expectMsg("1")
    receiveLogActor.expectMsg("2")
  }
}
