import akka.actor.Props
import app.EchoActor
import utils.Utils._
import utils.{BaseApp, Utils}

/**
  * Запуск Echo server
  */
object EchoApp extends BaseApp {
  val lineToCmd = Map("start" -> Start, "exit" -> Exit)

  override def createActor(params: AppParams) = Props(new EchoActor(params.getAddr))
}
