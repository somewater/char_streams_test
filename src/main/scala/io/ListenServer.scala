package io

import java.net.InetSocketAddress

import akka.actor.{Props, ActorLogging, ActorRef, Actor}
import akka.io.{Tcp, IO}
import akka.io.Tcp._
import akka.util.ByteString
import utils.CommandBuffer
import utils.Utils._

import scala.collection.mutable

/**
  * Сервер для отправки/получения данных. Используется через протокол в ListenServer
  */
class ListenServer(addr: InetSocketAddress, receiver: ActorRef) extends Actor with ActorLogging {

  import context.system

  val handlers = new mutable.HashMap[InetSocketAddress, ActorRef]()

  def bindPort: Unit = {
    IO(Tcp) ! Bind(self, addr)
    context.become(waitBound)
  }
  bindPort

  def receive = waitBound

  def waitBound: Receive = {
    case Bound(_) =>
      context.become(listen)
  }

  def listen: Receive = {
    case CommandFailed(cmd) =>
      log.error("IO command error: {}", cmd)

    case Connected(remote, local) =>
      val connection = sender()
      val handler = context.actorOf(Props(new ConnectionListener(remote, connection, receiver, self)))
      handlers.put(remote, handler)
      connection ! Register(handler)
      receiver ! ListenServer.Connected(remote)

    case d @ ListenServer.Disconnected(remote) =>
      receiver ! d
      handlers.remove(remote)

    case send @ ListenServer.Send(remote, data) =>
      handlers.get(remote) match {
        case Some(handler) =>
          handler ! send
        case None =>
          sender() ! CommandFailed(Write(ByteString(data)))
      }

    case msg =>
      log.error("Undefined message {}", msg)
  }
}

object ListenServer {
  case class Connected(remote: InetSocketAddress) // клиент установил соединение
  case class Disconnected(remote: InetSocketAddress) // соединение с клиентом потеряно
  case class Send(remote: InetSocketAddress, data: String) // отправить данные клиенту
  case class Received(remote: InetSocketAddress, data: String) // получены данные от клиента
}

private class ConnectionListener(remote: InetSocketAddress, connection: ActorRef, receiver: ActorRef, server: ActorRef) extends Actor {

  val cmdBuffer = new CommandBuffer

  def receive = {
    case Received(data) =>
      cmdBuffer.append(data.utf8String)
      cmdBuffer.extractCommands { cmd =>
        receiver ! ListenServer.Received(remote, cmd)
      }

    case ListenServer.Send(_, data) =>
      connection ! Write(ByteString(data + DELIMITER))

    case PeerClosed =>
      server ! ListenServer.Disconnected(remote)
      receiver ! ListenServer.Disconnected(remote)
      context.stop(self)
  }
}
