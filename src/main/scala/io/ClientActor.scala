package io

import java.net.InetSocketAddress
import akka.actor.{Stash, ActorRef, ActorLogging, Actor}
import akka.io.{IO, Tcp}
import Tcp._
import akka.util.ByteString
import utils.CommandBuffer
import utils.Utils._

/**
  * Клиент для отправки/получения данных в назначенный ip/port. Используется через протокол в ClientActor
  */
class ClientActor(addr: InetSocketAddress, receiver: ActorRef) extends Actor with ActorLogging with Stash {

  import context.system

  val cmdBuffer = new CommandBuffer
  var stashedMsgs = 0

  connect()

  def receive = waitConnection

  def connect() = {
    IO(Tcp) ! Connect(addr)
    context.become(waitConnection)
  }

  def waitConnection: Receive = {
    case Connected(_, _) =>
      sender() ! Register(self)
      unstashAll()
      stashedMsgs = 0
      context.become(waitData(sender))
    case ClientActor.Send(line) =>
      if (stashedMsgs < 10000) {
        stashedMsgs += 1
        stash()
      }
  }

  def waitData(connection: ActorRef): Receive = {
    case ClientActor.Send(line) =>
      connection ! Write(ByteString(line + DELIMITER))
    case Received(data) =>
      cmdBuffer.append(data.utf8String)
      cmdBuffer.extractCommands { cmd =>
        receiver ! ClientActor.Received(cmd)
      }

    case CommandFailed(cmd) =>
      log.error("IO cmd error: {}", cmd)
    case _: ConnectionClosed =>
      connect()
  }
}

object ClientActor {
  case class Send(line: String) // записать данные в сокет
  case class Received(line: String) // прочитаные из сокета данные
}
