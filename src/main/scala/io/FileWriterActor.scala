package io

import java.io.{Writer, FileWriter, PrintWriter}

import akka.actor.{Props, Actor}
import scala.concurrent.duration._
import FileWriterActor._

/**
  * Осуществляет запись в файл
  */
class FileWriterActor(filename: String) extends Actor {

  implicit val ex = context.system.dispatcher

  var writer: PrintWriter = _
  var flushNeeded = false

  override def preStart(): Unit = {
    super.preStart()
    writer = new PrintWriter(new FileWriter(filename, true), false)
    context.system.scheduler.schedule(3 seconds, 0.5 seconds, self, Flush)
  }

  override def postStop(): Unit = {
    super.postStop()
    writer.close()
  }

  def receive = {
    case line: String =>
      writer.println(line)
      flushNeeded = true

    case Flush =>
      if (flushNeeded) {
        writer.flush()
        flushNeeded = false
      }
  }
}

object FileWriterActor {
  def props(filename: String) = Props(classOf[FileWriterActor], filename)
  object Flush
}
