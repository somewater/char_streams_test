import akka.actor.Props
import app.ProxyActor
import utils.Utils._
import utils.{BaseApp, Utils}

/**
  * Запуск Proxy server
  */
object ProxyApp extends BaseApp {
  val lineToCmd = Map("start" -> Start, "exit" -> Exit)

  override def createActor(params: AppParams) = Props(new ProxyActor(params.getAddr))
}
