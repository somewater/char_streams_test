package utils

import Utils._

class CommandBuffer {
  val sb = StringBuilder.newBuilder

  def append(string: String) = {
    sb.append(string)
  }

  def extractCommands(handler: (String) => Unit): Unit = {
    val str = sb.result()
    def process(cmds: List[String]):String =
      if (cmds.isEmpty)
        ""
      else if (cmds.length == 1 && str.last != '\n')
        cmds.head
      else {
        handler(cmds.head)
        process(cmds.tail)
      }

    val lastCmdPart = process(str.split("\n").toList)
    sb.clear()
    sb.append(lastCmdPart)
  }
}
