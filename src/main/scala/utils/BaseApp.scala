package utils

import akka.actor.{Props, ActorSystem, ActorRef}
import utils.Utils.{AppParams, Cmd}

import scala.io.StdIn

/**
  * Темплейт для всех реализуемых серверных приложений, умеющий обрабатывать пользовательские команды
  */
abstract class BaseApp {

  val lineToCmd: Map[String, Cmd]

  def createActor(params: AppParams): Props

  def main(args: Array[String]) {
    val params = Utils.parseArgs(args)

    val system = ActorSystem(getClass.getSimpleName.replace("$", ""))
    val userActor = system.actorOf(createActor(params))
    system.actorOf(Props(classOf[akka.Main.Terminator], userActor), "app-terminator")

    processInput(userActor, system)
  }

  def processInput(actor: ActorRef, system: ActorSystem): Unit = {
    def getLine = StdIn.readLine().toLowerCase
    printHelp
    while (true) {
      val line = StdIn.readLine().toLowerCase
      lineToCmd.get(line) match {
        case Some(cmd) =>
          actor ! cmd
          if (cmd == Utils.Exit)
            return
        case None =>
          printHelp
      }
    }
  }

  def printHelp = println(s"Available commands: ${lineToCmd.keys.mkString(", ")}")
}
