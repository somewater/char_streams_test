package utils

import java.net.InetSocketAddress

object Utils {
  val DEBUG = System.getenv("DEBUG") != null

  def parseArgs(args: Array[String], withThreads: Boolean = true): AppParams = {
    if (args.size < 2)
      throw new RuntimeException(s"Wrong argumens, start as: scala App hostname port ${ if (withThreads) " [senderThreads] [receiverThreads]" else "" }")

    val argsOpt = args.lift
    AppParams(
      host = args(0),
      port = args(1).toInt,
      senderThreads = math.max(1, math.min(argsOpt(2).map(_.toInt).getOrElse(1), 3)),
      receiverThreads = math.max(1, math.min(argsOpt(3).map(_.toInt).getOrElse(1), 3)))
  }

  case class AppParams(host: String, port: Int, senderThreads: Int, receiverThreads: Int) {
    def getAddr: InetSocketAddress = new InetSocketAddress(host, port)
  }

  sealed trait Cmd
  object Start extends Cmd
  object Stop extends Cmd
  object Exit extends Cmd

  val START_MSG = "initiator_started"
  val STOP_MSG = "initiator_stopped"
  val EXIT_MSG = "initiator_exited"

  val DELIMITER = "\n" // tcp messages delimiter
}
