package app

import java.net.InetSocketAddress

import akka.actor.{ActorLogging, Actor, Props}
import io.{FileWriterActor, ClientActor}
import utils.Utils._

class EchoActor(proxyAddr: InetSocketAddress) extends Actor with ActorLogging {
  val client = context.actorOf(Props(new ClientActor(proxyAddr, self)))
  val receiveLog = context.actorOf(FileWriterActor.props("echo.txt"))

  def receive = {
    case Start =>
      context.become(started)
    case Exit =>
      context.stop(self)
  }

  def started: Receive = {
    case Exit =>
      context.stop(self)

    case ClientActor.Received(line) =>
      if (DEBUG)
        println(s"[echo] received ${line}")
      client ! ClientActor.Send(line)
      receiveLog ! line
  }
}
