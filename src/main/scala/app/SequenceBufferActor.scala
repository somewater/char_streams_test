package app

import akka.actor.{ActorRef, Actor}
import io.ClientActor
import scala.annotation.tailrec

/**
  * Буферизует сообщения, пока не будет получено последовательное сообщение.
  * Пересылает последовательные сообщения в receiver
  */
class SequenceBufferActor(first: String, receiver: ActorRef) extends Actor {

  var waited: String = first

  // храним в убывающем порядке. В общем случае сообщения приходят в возрастающем порядке, поэтому позицию вставки
  // ищем относительно начала очереди, а при попытке получить подходящее для отправки сообщение - ищем с конца
  val buffer = new java.util.LinkedList[String]

  def receive = {
    case ClientActor.Received(msg) =>
      if (msg == waited) {
        receiver ! ClientActor.Received(msg)
        waited = next(msg)
        while (!buffer.isEmpty && buffer.getLast == waited) {
          val last = buffer.pollLast
          receiver ! ClientActor.Received(last)
          waited = next(last)
        }
      } else {
        addMsg(msg)
      }
  }


  def addMsg(msg: String): Unit = {
    val iter = buffer.listIterator()
    while (iter.hasNext) {
      if (less(iter.next, msg)) {
        iter.add(msg)
        return
      }
    }
    buffer.addLast(msg)
  }

  def next(current: String): String = (current.toLong + 1).toString

  def less(a: String, b: String): Boolean = a.toLong < b.toLong
}
