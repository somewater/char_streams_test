package app

import java.net.InetSocketAddress

import akka.actor._
import app.InitiatorActor._
import io.{FileWriterActor, ClientActor}
import utils.Utils._

class InitiatorActor(proxyAddr: InetSocketAddress) extends Actor with ActorLogging {

  val sequenceBuffer = context.actorOf(Props(new SequenceBufferActor("0", self)))
  val client = context.actorOf(Props(new ClientActor(proxyAddr, sequenceBuffer)))
  val generator = new IntsGenerator(self)
  val sendLog = context.actorOf(FileWriterActor.props("initiator_send.txt"))
  val receiveLog = context.actorOf(FileWriterActor.props("initiator_receive.txt"))
  var lastSendedInt: Long = -1
  var lastReceivedInt: Long  = -1
  var alreadyStarted = false

  override def preStart(): Unit = {
    new Thread(generator).start()
  }

  override def postStop(): Unit = {
    generator.stop()
  }

  def receive = suspending


  override def unhandled(message: Any): Unit = message match {
    case Exit =>
      gotoFinalizing()

    case ClientActor.Received(line) =>
      receiveLine(line)

    case _ =>
      super.unhandled(message)
  }

  def suspending: Receive = {
    case Start =>
      if (alreadyStarted)
        client ! ClientActor.Send(START_MSG )
      alreadyStarted = true

      generator.start()
      context.become(working)
  }

  def working: Receive = {
    case Stop =>
      generator.pause()
      client ! ClientActor.Send(STOP_MSG)
      context.become(suspending)
    case i: Long =>
      if (DEBUG)
        println(s"[init] gen ${i}")
      client ! ClientActor.Send(i.toString)
      sendLog ! i.toString
      lastSendedInt = i

    case ClientActor.Received(line) =>
      receiveLine(line)
  }

  def finalizing: Receive = {
    case ClientActor.Received(line) =>
      receiveLine(line)
      if (line.toLong == lastSendedInt)
        stop()
  }

  def gotoFinalizing() = {
    generator.stop()
    client ! ClientActor.Send(EXIT_MSG)
    if (lastSendedInt == lastReceivedInt)
      stop()
    else
      context.become(finalizing)
  }

  def receiveLine(line: String) = {
    if (DEBUG)
      println(s"[init] received ${line}")
    receiveLog ! line
    lastReceivedInt = line.toLong
  }

  def stop() = {
    context.stop(self)
    println("[init] gracefully stopped")
  }
}

object InitiatorActor {
  sealed trait State
  object Suspending extends State
  object Working extends State
  object Finalizing extends State

  object Data
}

class IntsGenerator(receiver: ActorRef) extends Runnable {
  @volatile private var running = false
  @volatile private var stopped = false

  override def run(): Unit = {
    var i = 0L
    while (!stopped) {
      while (running && !stopped) {
        receiver ! i
        i += 1
        Thread.sleep(1)
      }
    }
  }

  def stop() = stopped = true

  def pause() = running = false

  def start() = running = true
}