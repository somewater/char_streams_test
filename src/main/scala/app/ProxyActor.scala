package app

import java.net.InetSocketAddress

import akka.actor.{Props, Actor}
import io.ListenServer
import utils.Utils._

class ProxyActor(addr: InetSocketAddress) extends Actor {

  var clients: Set[InetSocketAddress] = Set.empty

  val server = context.actorOf(Props(new ListenServer(addr, self)))

  def receive = {
    case Start =>
      context.become(started)
  }

  def started: Receive = {
    case ListenServer.Received(remote, line) =>
      line match {
        case START_MSG | STOP_MSG | EXIT_MSG =>
          println("[proxy] " + line.replace("_", " "))
        case _ =>
          resendLine(remote, line)
      }
  }


  override def unhandled(message: Any): Unit = message match {
    case ListenServer.Connected(remote) =>
      if (DEBUG)
        println(s"[proxy] client $remote connected")
      clients = clients + remote
    case ListenServer.Disconnected(remote) =>
      if (DEBUG)
        println(s"[proxy] client $remote disconnected")
      clients = clients - remote
    case Exit =>
      context.stop(self)
    case _ =>
      super.unhandled(message)
  }

  def resendLine(sender: InetSocketAddress, line: String) = {
    if (DEBUG)
      println(s"[proxy] received ${line}, send to ${clients.filter(_ != sender).mkString(",")}")
    clients.foreach {
      remote =>
        if (remote != sender)
          server ! ListenServer.Send(remote, line)
    }
  }
}
