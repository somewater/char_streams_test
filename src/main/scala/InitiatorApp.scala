import akka.actor.{ActorRef, Props, ActorSystem}
import app.InitiatorActor
import app.InitiatorActor._
import utils.{BaseApp, Utils}

import scala.io.StdIn
import Utils._
import akka.dispatch.BoundedMailbox

/**
  * Запуск Initiator server
  */
object InitiatorApp extends BaseApp {

  val lineToCmd = Map("start" -> Start, "stop" -> Stop, "exit" -> Exit)

  override def createActor(params: AppParams) = Props(new InitiatorActor(params.getAddr)).withMailbox("bounded-mailbox")
}
